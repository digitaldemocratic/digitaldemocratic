# Stress tests

By executing tests in this normalised fashion it is easier to compare results
between different instances or different patch levels.

This documents normalised stress-testing and references files under
[`stress-tests`][st].

[st]: https://gitlab.com/DD-workspace/DD/-/tree/main/stress-tests


## VM tests

- [`vm-tests.sh`][vm-tests.sh]: generate a text file to compare CPU and other
  factors across VM types, providers or instances which may affect DD
  performance.

We can compare the resulting logs just with, e.g. `vim -d`.

[vm-tests.sh]: https://gitlab.com/DD-workspace/DD/-/tree/main/stress-tests/vm-tests.sh

## DD tests

Currently these tests perform logins and interact with Nextcloud, but it would
be interesting to expand them to interact with other services.

### Directory contents

This directory contains following files:

- [`dd-stress-test.tpl.jmx`][dd-stress-test.tpl.jmx]: template to generate
  [JMeter][jm] tests to execute
- [`dd-tests.sh`][dd-tests.sh]: helper script that generates the actual test
  plan files and executes them. See `./dd-tests.sh --help`
- [`dd-tests-selenium.py`][dd-tests-selenium.py]: this gives us an idea of
  how a user would perceive DD to be behaving while under load.
  Called by `./dd-tests.sh` by default.

[dd-stress-test.tpl.jmx]: https://gitlab.com/DD-workspace/DD/-/tree/main/stress-tests/dd-stress-test.tpl.jmx
[dd-tests.sh]: https://gitlab.com/DD-workspace/DD/-/tree/main/stress-tests/dd-tests.sh
[dd-tests-selenium.py]: https://gitlab.com/DD-workspace/DD/-/tree/main/stress-tests/dd-tests-selenium.py

### Results

Results will be saved in a `results` directory, where each subdirectory
corresponds to a stress test executed with `dd-test.sh`.

The naming scheme for those subdirectories is: `DOMAIN_THREADCOUNT_DURATION`
Where `THREADCOUNT` and `DURATION` are the corresponding [JMeter][jm]
parameters.

Each results directory contains:

- `log`: the [JMeter][jm] log
- `results`: the [JMeter][jm] results file
- `html/index.html`: the interactive graphs for the data as produced by JMeter
- `selenium/session.html`: the session report as would be perceived by a user.
  Note this requires Python3 and selenium and can be disabled by setting the
  environment variable: `USE_SELENIUM=NO`.

[jm]: https://jmeter.apache.org/
