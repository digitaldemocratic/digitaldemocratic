# Contribuint

DD és programari lliure sota la llicència AGPL3+, les contribucions de qualsevol
persona són benvingudes.

## El codi

El repositori de codi i documentació tècnica es troba a:
[https://gitlab.com/DD-workspace/DD](https://gitlab.com/DD-workspace/DD)

## L'arquitectura del DD

Per molts canvis serà necessari que us familiaritzeu amb l'arquitectura del DD.

El diagrama d'arquitectura del DD es troba al repositori en format
[editable][diagramedit], i hi podeu contribuir com amb qualsevol altre canvi.

![DD Workspace for education - Arquitectura](assets/diagram/dd_workspace_education.png)

[diagramedit]: https://gitlab.com/DD-workspace/DD/-/tree/main/docs/assets/diagram


## Metodología: Desenvolupament Adaptatiu de Software (ASD)

Per tal d'organitzar-nos fem servir la metodologia de
Desenvolupament Adaptatiu de Software
(o ASD per les seves sigles en anglès Adaptive Software Development).

Aquesta metodologia posa el focus en les dinàmiques d'equips auto-organitzats,
la col·laboració interpersonal i l'aprenentatge individual i dels equips
de manera que ens permet integrar fàcilment la naturalesa *Open Source*
del **DD** amb els cicles de feedback o d'actualització a les escoles que fan
part del projecte Pilot i les necessitats tècniques del projecte.

ASD ajuda a construir sistemes de software complexes, en el nostre cas un
entorn educatiu digital.

S'organitza en cicles iteratius de 3 fases:

1. **Especulació**: En aquesta fase planegem tenint en compte les limitacions i
  necessitats del projecte, dels usuaris, etc. (en forma de [Issues][issues],
  feedback o incidències) per tal de definir els cicles que volem.
2. **Col·laboració**: En aquesta fase ens organitzem i treballem conjuntament,
  sempre tenint en compte que les persones que treballen juntes han de:

    - Criticar sense hostilitat
    - Assistir sense ressentiment
    - Treballar el més fortament possible
    - Tenir o obtenir les habilitats necessàries
    - Comunicar els problemes que eviten trobar una solució efectiva

3. **Aprenentatge**: Tant si arribem al resultat desitjat com si no, aquest
  procés haurà incrementat l'enteniment del projecte i les tecnologies
  associades de totes les persones que hi participen.
  En aquest procés una part crucial són les [Merge Request][mr] i les revisions
  (**Reviews**) associades, així com la comunicació de l'equip que hi treballa.

Aplicant aquesta metodologia de manera iterativa anem millorant el projecte
incrementalment.

Alguns recursos online gratuïts per familiaritzar-s'hi són:

- [https://users.exa.unicen.edu.ar/catedras/agilem/cap23asd.pdf](https://users.exa.unicen.edu.ar/catedras/agilem/cap23asd.pdf)
- [https://www.geeksforgeeks.org/adaptive-software-development-asd/](https://www.geeksforgeeks.org/adaptive-software-development-asd/)
- [https://yewtu.be/watch?v=HXJWwxL2N5A](https://yewtu.be/watch?v=HXJWwxL2N5A)
- [https://files.ifi.uzh.ch/rerg/amadeus/teaching/seminars/seminar_ws0304/06_Eberle_ASD_Folien.pdf](https://files.ifi.uzh.ch/rerg/amadeus/teaching/seminars/seminar_ws0304/06_Eberle_ASD_Folien.pdf)

### En la pràctica

- Has detectat un problema? Ajuda'ns reportant un [problema/issue][issues].
- Vols afegir un parche? Et pot ser útil l'apartat dels controls de qualitat
  tot seguit. En funció de la complexitat dels teus canvis:

    - Si en són senzills, obre directament una [Merge Request][mr]
      explicant el que vols afegir i per què.
      D'aquesta manera iniciarà el procés de **revisió/review**.
    - Si en són més complicats, potser posa't en contacte abans perquè puguem
      avaluar si la teva solució és la que més s'adjusta al projecte.
      Una bona manera de fer-ho és obrint una [issue][issues] explicant el que
      vols fer, per què i com.

- Col·labores habitualment amb el DD? Ajuda amb les revisions de codi de les
  [Merge Request][mr] pendents, tenint en compte els controls de qualitat.

[issues]: https://gitlab.com/DD-workspace/DD/-/issues
[mr]: https://gitlab.com/DD-workspace/DD/-/merge_requests

## Controls de qualitat

### Continuous Integration

Per simplificar la feina de revisió de codi i assegurar una certa qualitat del
que entra al repositori:

- Ningú pot fer `push` a la branca principal (`main`) directament.
  Sempre és necessari passar pel procés de [Merge Request][mr] i review.
- Disposem d'una instància de [buildbot][buildbot] com a Continuous Integration
  (a `ci.dd-work.space`, login amb GitLab) que reacciona a esdeveniments de
  tipus `push` al repositori principal, i de tipus [Merge Request][mr] des de
  qualsevol repositori.
    - Quan es tracta d'un [Merge Request][mr], només s'executen comprovacions
      de estàtiques, que no executen el codi del repositori.
      Això és per evitar abús de tipus mineria de criptomonedes i demés.
      Aquestes comprovacions ara mateix són:
      [ShellCheck][sc] per scripts de shell, [mkdocs][mkdocs] per comprovar
      que la documentació es genera correctament, i aviat hi afegirem linters i
      comprovadors estàndard de Python.
    - Quan es tracta d'un esdeveniment `push` al repositori principal,
      addicionalment a les comprovacions anteriors, es comença el procés
      d'instal·lació des de zero en una màquina virtual.
      Això ajuda a detectar quan s'introdueixen problemes i a assegurar que el
      DD es pot instal·lar correctament.
- Els membres del grup `DD-workspace` a GitLab tenen permisos d'administració a
  la instància de Buildbot, això els permet iniciar builds manualment o
  cancel·lar tasques si és necessari.

### Checklist

Abans d'aprovar una Merge Request, hem de fer les següents comprovacions als
canvis que es volen introduir:

- [ ] Passen les comprovacions a la CI
- [ ] Les modificacions introduïdes són necessàries, solucionen un problema
      real, o milloren la mantenibilitat del projecte
- [ ] Es consideren i debaten les implicacions de **seguretat**
- [ ] Es consideren i debaten les implicacions de **mantenibilitat**
- [ ] Es revisen possibles regressions de funcionalitat
- [ ] No s'utilitzen dades reals en les proves
- [ ] El resultat de la instal·lació des de zero en el CI és satisfactori
    - [ ] És possible crear i modificar grups i usuaris
    - [ ] És possible iniciar sessió amb SSO a Moodle, Nextcloud i Wordpress
- [ ] Es respecta el [Principi de mínima sorpresa (Principle Of Least Attonishment / POLA)][pola]
    - [ ] Això inclou que es respectin les configuracions explícites
          dels operadors (`dd.conf`)
    - [ ] Les funcionalitats noves que puguin tenir efectes negatius, només
          s'aplicaran per defecte, quan els operadors hagin tingut temps
          suficient de desactivar-les abans


Si algun d'aquests passos falla, procedirem a ajudar a qui ha proposat els
canvis a solucionar els inconvenients.

[buildbot]: https://buildbot.net
[pola]: https://en.wikipedia.org/wiki/Principle_of_least_astonishment
