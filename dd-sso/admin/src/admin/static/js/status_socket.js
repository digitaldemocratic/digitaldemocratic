//
//   Copyright © 2021,2022 IsardVDI S.L.
//
//   This file is part of DD
//
//   DD is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or (at your
//   option) any later version.
//
//   DD is distributed in the hope that it will be useful, but WITHOUT ANY
//   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//   details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with DD. If not, see <https://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL-3.0-or-later

notice={}
$lost=0;

socket = io.connect(location.protocol+'//' + document.domain +'/sio');
console.log(location.protocol+'//' + document.domain +'/sio')
socket.on('connect', function() {
    if($lost){location.reload();}
    console.log('Listening status socket');
});

socket.on('connect_error', function(data) {
    $lost=$lost+1;
    $('#modal-lostconnection').modal({
        backdrop: 'static',
        keyboard: false
    }).modal('show');
});

socket.on('notify-create', function(data) {
    var data = JSON.parse(data);
    notice[data.id] = new PNotify({
        title: data.title,
        text: data.text,
        hide: false,
        type: data.type
    });
});

socket.on('notify-destroy', function(data) {
    var data = JSON.parse(data);
    notice[data.id].remove()
});

socket.on('notify-increment', function(data) {
    var data = JSON.parse(data);
    if(!( data.id in notice)){
        notice[data.id] = new PNotify({
            title: data.title,
            text: data.text,
            hide: false,
            type: data.type
        });
    }
    // console.log(data.text)
    notice[data.id].update({
        text: data.text
    })
    if(! data.table == false){
        dtUpdateInsert(data.table,data['data']['data'])
    }
});



    // new PNotify({
    //     title: "Quota for creating desktops full.",
    //     text: "Can't create another desktop, user quota full.",
    //     hide: true,
    //     delay: 3000,
    //     icon: 'fa fa-alert-sign',
    //     opacity: 1,
    //     type: 'error'
    // });

// socket.on('update', function(data) {
//     var data = JSON.parse(data);
//     console.log('Status update')
//     console.log(data)
//     // var data = JSON.parse(data);
//     // drawUserQuota(data);
// });

socket.on('update', function(data) {
    var data = JSON.parse(data);
    console.log('Status update')
    console.log(data)
    // var data = JSON.parse(data);
    // drawUserQuota(data);
});

    // {'event':'traceback',
    // 'id':u['id'],
    // 'item':'group',
    // 'action':'add'
    // 'name':g['name'],
    // 'progress':str(item)+'/'+str(total),
    // 'status':False,
    // 'msg':,
    // 'payload':{'traceback':traceback.format_exc(),
    //             'data':g})

socket.on('progress', function(data) {
    var data = JSON.parse(data);
    console.log(data)
    // $('.modal-progress #item').html(data.item)
});



//// 
